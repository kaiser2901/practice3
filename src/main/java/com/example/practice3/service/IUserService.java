package com.example.practice3.service;

import com.example.practice3.DTO.PaginationModelRequest;
import com.example.practice3.entity.User;
import org.springframework.data.domain.Page;

import java.util.List;

public interface IUserService {
    User getUserByUsername(String username);
    Page<User> getAllWithPagination(PaginationModelRequest request);
    User upsert(User user);
    void delete(Long userId);
    List<User> getUsers();
    void addRoleToUser(String username, String roleName);
    List<String> getAllUsernames();
}
