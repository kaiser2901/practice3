package com.example.practice3.service;

import com.example.practice3.entity.Department;

import java.util.List;

public interface IDepartmentService {

    List<Department> getAll();

    Department save(Department department);

    void delete(Long departmentId);

    Department findById(Long departmentId);

    Department findByCode(String code);
}
