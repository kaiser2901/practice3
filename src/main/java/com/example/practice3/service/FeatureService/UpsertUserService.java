package com.example.practice3.service.FeatureService;

import com.example.practice3.DTO.UserModelRequest;
import com.example.practice3.entity.Role;
import com.example.practice3.entity.User;
import com.example.practice3.exception.ApiRequestException;
import com.example.practice3.service.IRoleService;
import com.example.practice3.service.IUpsertUserService;
import com.example.practice3.service.IUserService;
import com.example.practice3.service.IValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;

@Service
public class UpsertUserService implements IUpsertUserService {

    @Autowired
    private IUserService userService;
    @Autowired
    private IValidationService validationService;
    @Autowired
    private IRoleService roleService;
    

    @Override
    public User upsertUser(UserModelRequest request) {
        try {
            Collection<Role> roles = new ArrayList<>();
            User user = userService.getUserByUsername(request.getUserUsername());
            if(validationService.checkNullOrEmpty(user)){
                user = new User();
            }

            if(!validationService.checkNullOrEmpty(request.getRoleName())){
                roles = new ArrayList<>();
                for(String roleName : request.getRoleName()){
                    Role value = roleService.findByName(roleName);
                    roles.add(value);
                }
            } else {
                Role value = roleService.findByName("ROLE_USER");
                roles.add(value);
            }

            user = User.builder()
                    .userUsername(request.getUserUsername())
                    .userName(request.getUserName())
                    .userPassword(request.getUserPassword())
                    .roles(roles)
                    .build();

            return user;
        } catch (Exception e) {
            throw new ApiRequestException(e.getMessage());
        }
    }
}
