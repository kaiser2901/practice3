package com.example.practice3.service.FeatureService;

import com.example.practice3.DTO.PaginationAndSortModelRequest;
import com.example.practice3.exception.ApiRequestException;
import com.example.practice3.service.IPaginationAndSortService;
import com.example.practice3.service.IValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class PaginationAndSortService implements IPaginationAndSortService {

    @Autowired
    private IValidationService validationService;

    @Override
    public PaginationAndSortModelRequest validationPaginationAndSort(PaginationAndSortModelRequest request) {

        int size = request.getPageSize();
        int page = request.getPage();
        String field = request.getField();
        int sizeDefault = 5;
        int pageDefault = 0;
        List<String> fieldEmployee = Arrays.asList("employeeId", "employeeName", "department");
        boolean checkEmployee = false;

        try {
            //check null request param -> if null -> get default value
            size = validationService.checkNullOrEmpty(size) || size < sizeDefault ? sizeDefault : size;
            page = validationService.checkNullOrEmpty(page) || page <= 0 ? pageDefault : page - 1;
            field = validationService.checkNullOrEmpty(field) ? fieldEmployee.get(0) : field;

            for (int i = 0; i < fieldEmployee.size(); i++) {
                if (field.equalsIgnoreCase(fieldEmployee.get(i))) {
                    field = fieldEmployee.get(i);
                    checkEmployee = true;
                    break;
                }
            }
            if (checkEmployee = false) {
                field = fieldEmployee.get(0);
            }

            return new PaginationAndSortModelRequest(page, size, field);

        } catch (Exception e) {
            throw new ApiRequestException(e.getMessage());
        }

    }
}
