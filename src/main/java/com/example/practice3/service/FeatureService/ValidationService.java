package com.example.practice3.service.FeatureService;

import com.example.practice3.exception.ApiRequestException;
import com.example.practice3.service.IValidationService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ValidationService<T> implements IValidationService<T> {

    @Override
    public boolean checkNullOrEmpty(T input) {
        return input == null || input.toString().isEmpty();
    }

    @Override
    public boolean checkList(List<T> input) {
        return input.isEmpty();
    }
}
