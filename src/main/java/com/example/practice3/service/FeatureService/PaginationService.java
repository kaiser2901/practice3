package com.example.practice3.service.FeatureService;

import com.example.practice3.DTO.PaginationModelRequest;
import com.example.practice3.exception.ApiRequestException;
import com.example.practice3.service.IPaginationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PaginationService implements IPaginationService {
    @Autowired
    private ValidationService validationService;

    @Override
    public PaginationModelRequest validatePagination(PaginationModelRequest request) {
        int size = request.getPageSize();
        int page = request.getPage();
        int sizeDefault = 5;
        int pageDefault = 0;

        try {
            size = validationService.checkNullOrEmpty(size) || size < sizeDefault ? sizeDefault : size;
            page = validationService.checkNullOrEmpty(page) || page <= 0 ? pageDefault : page - 1;

            return new PaginationModelRequest(page, size);
        } catch (Exception e) {
            throw new ApiRequestException(e.getMessage());
        }
    }
}
