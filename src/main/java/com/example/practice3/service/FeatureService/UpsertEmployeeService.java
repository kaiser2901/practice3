package com.example.practice3.service.FeatureService;

import com.example.practice3.DTO.EmployeeModelRequest;
import com.example.practice3.entity.Employee;
import com.example.practice3.exception.ApiRequestException;
import com.example.practice3.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class UpsertEmployeeService implements IUpsertEmployeeService {

    @Autowired
    private IEmployeeService employeeService;
    @Autowired
    private IDepartmentService departmentService;
    @Autowired
    private IValidationService validationService;

    @Override
    public Employee upsert(EmployeeModelRequest request) {
        try {
            Employee employee = employeeService.findById(request.getEmployeeId());

            if (validationService.checkNullOrEmpty(employee)) {
                employee = new Employee();
                employee.setEmployeeCode(UUID.randomUUID().toString());
            }
            employee.setEmployeeEmail(request.getEmployeeEmail());
            employee.setEmployeeName(request.getEmployeeName());
            employee.setEmployeePhone(request.getEmployeePhone());
            employee.setDepartment(departmentService.findById(request.getDepartmentId()));

            return employeeService.save(employee);
        } catch (Exception e) {
            throw new ApiRequestException(e.getMessage());
        }
    }
}
