package com.example.practice3.service.FeatureService;

import com.example.practice3.DTO.DepartmentModelRequest;
import com.example.practice3.entity.Department;
import com.example.practice3.exception.ApiRequestException;
import com.example.practice3.service.IDepartmentService;
import com.example.practice3.service.IUpsertDepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class UpsertDepartmentService implements IUpsertDepartmentService {
    @Autowired
    private IDepartmentService departmentService;

    @Override
    public Department upsert(DepartmentModelRequest request) {
        try{
            Department department = departmentService.findById(request.getDepartmentId());

            if(department == null){
                department = new Department();
                department.setDepartmentCode(UUID.randomUUID().toString());
            }

            department.setDepartmentName(request.getDepartmentName());

            return departmentService.save(department);
        }catch (Exception e){
            throw new ApiRequestException(e.getMessage());
        }
    }
}
