package com.example.practice3.service;

import com.example.practice3.DTO.PaginationModelRequest;
import com.example.practice3.entity.Role;
import org.springframework.data.domain.Page;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface IRoleService {
//    Collection<Role> findByUsername(Collection<String> username);
    List<Role> getAll();
    Page<Role> getAllWithPagination(PaginationModelRequest paginationModelRequest);
    Role findById(Long id);
    Role findByName(String name);
    Role save(Role role);
    void delete(Long roleId);

}
