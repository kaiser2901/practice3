package com.example.practice3.service;

import com.example.practice3.DTO.EmployeeModelRequest;
import com.example.practice3.entity.Employee;


public interface IUpsertEmployeeService {

    Employee upsert(EmployeeModelRequest request);

}
