package com.example.practice3.service;

import com.example.practice3.DTO.PaginationModelRequest;

public interface IPaginationService {
    PaginationModelRequest validatePagination(PaginationModelRequest request);
}
