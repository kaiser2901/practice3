package com.example.practice3.service;

import java.util.List;

public interface IValidationService<T> {

    boolean checkNullOrEmpty(T check);

    boolean checkList(List<T> list);

}
