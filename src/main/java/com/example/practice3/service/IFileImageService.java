package com.example.practice3.service;

import com.example.practice3.DTO.FileImageModelRequest;
import com.example.practice3.entity.FileImage;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Optional;
import java.util.stream.Stream;

public interface IFileImageService {

    FileImage upload(MultipartFile file) throws IOException;

    Stream<FileImage> getAll();

    Optional<FileImage> findById(String id);

    FileImageModelRequest findFileImageById(String id);

    Stream<FileImage> getAllByEmployeeId(Long employeeId);

    void delete(String id);


}
