package com.example.practice3.service;

import com.example.practice3.DTO.UserModelRequest;
import com.example.practice3.entity.User;

public interface IUpsertUserService {
    User upsertUser(UserModelRequest request);
}
