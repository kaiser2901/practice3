package com.example.practice3.service;

import com.example.practice3.DTO.PaginationAndSortModelRequest;
import com.example.practice3.DTO.PaginationModelRequest;
import com.example.practice3.entity.Employee;
import org.springframework.data.domain.Page;

import java.util.List;

public interface IEmployeeService {

    List<Employee> getAll();

    List<Employee> findWithSortingASC(String field);

    List<Employee> findWithSortingDESC(String field);

    Page<Employee> findWithPagination(PaginationModelRequest request);

    Page<Employee> findWithPaginationAndSorting(PaginationAndSortModelRequest request);

    Employee save(Employee employee);

    void delete(Long employeeId);

    Employee findById(Long employeeId);

    Employee mergeFileImage(Long employeeId, String FileImageId);


}
