package com.example.practice3.service;

import com.example.practice3.DTO.DepartmentModelRequest;
import com.example.practice3.entity.Department;

public interface IUpsertDepartmentService {

    Department upsert(DepartmentModelRequest request);

}
