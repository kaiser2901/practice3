package com.example.practice3.service;

import com.example.practice3.DTO.PaginationAndSortModelRequest;

public interface IPaginationAndSortService {
    PaginationAndSortModelRequest validationPaginationAndSort(PaginationAndSortModelRequest request);
}
