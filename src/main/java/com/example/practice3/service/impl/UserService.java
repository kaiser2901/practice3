package com.example.practice3.service.impl;

import com.example.practice3.DTO.PaginationModelRequest;
import com.example.practice3.entity.Role;
import com.example.practice3.entity.User;
import com.example.practice3.exception.ApiRequestException;
import com.example.practice3.repo.UserRepo;
import com.example.practice3.service.FeatureService.ValidationService;
import com.example.practice3.service.IUserService;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
@Transactional
@Slf4j
public class UserService implements IUserService, UserDetailsService {

    @Autowired
    private UserRepo userRepo;
    @Autowired
    private RoleService roleService;
    @Autowired
    private ValidationService<Object> validationService;
    @Autowired
    private PasswordEncoder passwordEncoder;

    private static final Logger logger = LogManager.getLogger(UserService.class);

    @Override
    public User getUserByUsername(String username) {
        try {
            logger.info("You get user {}",() -> username);
            return userRepo.findByUserUsername(username).orElse(null);
        } catch (Exception e) {
            throw new ApiRequestException(e.getMessage());
        }
    }

    @Override
    public Page<User> getAllWithPagination(PaginationModelRequest request) {
        try {
            logger.info("You get all user with pagination");
            return userRepo.findAll(PageRequest.of(request.getPage(), request.getPageSize()));
        } catch (Exception e) {
            throw new ApiRequestException(e.getMessage());
        }
    }

    @Override
    public User upsert(User user) {
        try {
            List<String> listUsername = getAllUsernames();
            for (String username : listUsername) {
                if(username.equals(user.getUserUsername())) {
                    throw new ApiRequestException("User ...");
                }
            }
            logger.info("Your action with user {} complete",() -> user.getUserUsername());
            user.setUserPassword(passwordEncoder.encode(user.getUserPassword()));
            return userRepo.save(user);
        } catch (Exception e) {
            throw new ApiRequestException(e.getMessage());
        }
    }

    @Override
    public void delete(Long userId) {
        try {
            logger.info("You delete user {}",() -> userId);
            userRepo.deleteById(userId);
        } catch (Exception e) {
            throw new ApiRequestException(e.getMessage());
        }
    }

    @Override
    public List<User> getUsers() {
        try {
            return userRepo.findAll();
        } catch (Exception e) {
            throw new ApiRequestException(e.getMessage());
        }
    }

    @Override
    public void addRoleToUser(String username, String roleName) {
        try {
            User user = getUserByUsername(username);
            Role role = roleService.findByName(roleName);
            if(validationService.checkNullOrEmpty(user) || validationService.checkNullOrEmpty(role)){
                throw new ApiRequestException("Something went wrong...");
            }
            user.getRoles().add(role);
        } catch (Exception e) {
            throw new ApiRequestException(e.getMessage());
        }
    }

    @Override
    public List<String> getAllUsernames() {
        try {
            List<User> userList = userRepo.findAll();
            List<String> usernameList = new ArrayList<String>();
            for(User user : userList){
                usernameList.add(user.getUserUsername());
            }
            return usernameList;
        } catch (Exception e) {
            throw new ApiRequestException(e.getMessage());
        }
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepo.findByUserUsername(username).orElse(null);
        if(validationService.checkNullOrEmpty(user)) {
            log.error("User not found in the database");
            throw new ApiRequestException("User not found in the database");
        } else {
            log.info("User found in the database: {}", username);
        }
        Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
        user.getRoles().forEach(
                role -> authorities.add(new SimpleGrantedAuthority(role.getRoleName()))
        );
        return new org.springframework.security.core.userdetails.User(user.getUserUsername(),user.getUserPassword(), authorities);
    }
}
