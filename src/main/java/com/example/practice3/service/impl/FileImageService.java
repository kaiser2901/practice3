package com.example.practice3.service.impl;

import com.example.practice3.DTO.FileImageModelRequest;
import com.example.practice3.entity.FileImage;
import com.example.practice3.exception.ApiRequestException;
import com.example.practice3.repo.FileImageRepo;
import com.example.practice3.service.IFileImageService;
import com.example.practice3.service.IValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Optional;
import java.util.stream.Stream;

@Service
public class FileImageService implements IFileImageService {

    @Autowired
    private FileImageRepo fileImageRepo;
    @Autowired
    private IValidationService validationService;

    @Override
    public FileImage upload(MultipartFile file) throws IOException {
        try{
            String fileName = StringUtils.cleanPath(file.getOriginalFilename());
            FileImage fileImage = new FileImage(fileName, file.getContentType(), file.getBytes());
            return fileImageRepo.save(fileImage);
        }catch (Exception e){
            throw new ApiRequestException(e.getMessage());
        }
    }

    @Override
    public Stream<FileImage> getAll() {
        try{
            return fileImageRepo.findAll().stream();
        }catch (Exception e){
            throw new ApiRequestException(e.getMessage());
        }
    }

    @Override
    public Optional<FileImage> findById(String id) {
        try{
            return fileImageRepo.findById(id);
        }catch (Exception e){
            throw new ApiRequestException(e.getMessage());
        }
    }

    @Override
    public FileImageModelRequest findFileImageById(String id) {
        try{
            FileImage fileImage = fileImageRepo.findById(id).orElse(null);
            FileImageModelRequest fileImageModelRequest = new FileImageModelRequest();
            fileImageModelRequest.setFileId(fileImage.getFileId());
            fileImageModelRequest.setFileName(fileImage.getFileName());
            fileImageModelRequest.setFileData(fileImage.getFileData());
            fileImageModelRequest.setFileType(fileImage.getFileType());
            return fileImageModelRequest;
        }catch (Exception e){
            throw new ApiRequestException(e.getMessage());
        }

    }

    @Override
    public Stream<FileImage> getAllByEmployeeId(Long employeeId) {
        try{
            return fileImageRepo.findAllByEmployee_EmployeeId(employeeId).stream();
        }catch (Exception e){
            throw new ApiRequestException(e.getMessage());
        }
    }

    @Override
    public void delete(String id) {
        try{
            fileImageRepo.deleteById(id);
        }catch (Exception e){
            throw new ApiRequestException(e.getMessage());
        }
    }
}
