package com.example.practice3.service.impl;

import com.example.practice3.DTO.PaginationAndSortModelRequest;
import com.example.practice3.DTO.PaginationModelRequest;
import com.example.practice3.entity.Employee;
import com.example.practice3.entity.FileImage;
import com.example.practice3.exception.ApiRequestException;
import com.example.practice3.repo.EmployeeRepo;
import com.example.practice3.repo.FileImageRepo;
import com.example.practice3.service.IEmployeeService;
import com.example.practice3.service.IValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService implements IEmployeeService {
    @Autowired
    private EmployeeRepo employeeRepo;
    @Autowired
    private FileImageRepo fileImageRepo;
    @Autowired
    private IValidationService validationService;

    @Override
    public List<Employee> getAll(){
        List<Employee> employee = employeeRepo.findAll();
        try{
            return employee;
        }catch (Exception e){
            if (employee == null){
                throw new ApiRequestException("No Employee");
            }
            throw new ApiRequestException("Something went wrong...");
        }
    }

    @Override
    public List<Employee> findWithSortingASC(String field) {
        try{
            return employeeRepo.findAll(Sort.by(Sort.Direction.ASC, field));
        }catch (Exception e){
            throw new ApiRequestException(e.getMessage());
        }
    }

    @Override
    public List<Employee> findWithSortingDESC(String field) {
        try{
            return employeeRepo.findAll(Sort.by(Sort.Direction.DESC, field));
        }catch (Exception e){
            throw new ApiRequestException(e.getMessage());
        }
    }

    @Override
    public Page<Employee> findWithPagination(PaginationModelRequest request) {
        try{
            return employeeRepo.findAll(PageRequest.of(request.getPage(), request.getPageSize()));
        }catch (Exception e){
            throw new ApiRequestException(e.getMessage());
        }
    }

    @Override
    public Page<Employee> findWithPaginationAndSorting(PaginationAndSortModelRequest request) {
        try{
            return employeeRepo.findAll(PageRequest.of(request.getPage(), request.getPageSize()).withSort(Sort.by(request.getField())));
        }catch (Exception e){
            throw new ApiRequestException(e.getMessage());
        }
    }

    //Upsert Employee
    @Override
    public Employee save(Employee employee){
        try{
            return employeeRepo.save(employee);
        }catch (Exception e){
            throw new ApiRequestException(e.getMessage());
        }
    }

    @Override
    public void delete(Long employeeId){
        try{
            employeeRepo.deleteById(employeeId);
        }catch (Exception e){
            throw new ApiRequestException(e.getMessage());
        }
    }

    @Override
    public Employee findById(Long employeeId){
        try{
            return employeeRepo.findEmployeeByEmployeeId(employeeId).orElse(null);
        }catch (Exception e){
            throw new ApiRequestException(e.getMessage());
        }
    }

    @Override
    public Employee mergeFileImage(Long employeeId, String fileImageId) {
        try{
            Employee employee = findById(employeeId);
            FileImage fileImage = fileImageRepo.findById(fileImageId).orElse(null);
            if(validationService.checkNullOrEmpty(fileImage)){
                throw new ApiRequestException("Can't find Image, something went wrong...");
            }
            if(validationService.checkNullOrEmpty(employee.getFileImage()) == false){
                fileImageRepo.deleteById(employee.getFileImage().getFileId());
            }
            employee.setFileImage(fileImage);
            fileImage.setEmployee(employee);
            employeeRepo.save(employee);
            fileImageRepo.save(fileImage);
            return employee;
        }catch (Exception e){
            throw new ApiRequestException(e.getMessage());
        }
    }
}
