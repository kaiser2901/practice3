package com.example.practice3.service.impl;

import com.example.practice3.entity.Department;
import com.example.practice3.exception.ApiExceptionHandler;
import com.example.practice3.exception.ApiRequestException;
import com.example.practice3.repo.DepartmentRepo;
import com.example.practice3.service.IDepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DepartmentService implements IDepartmentService {
    @Autowired
    private  DepartmentRepo departmentRepo;

    //Get List Department
    @Override
    public List<Department> getAll() {
        try {
            List<Department> departments = departmentRepo.findAll();
            return departments;
        }catch (Exception e) {
            throw new ApiRequestException(e.getMessage());
        }
    }

    //Upsert Department
    @Override
    public Department save(Department department) {
        try {
            return departmentRepo.save(department);
        }catch (Exception e) {
            throw new ApiRequestException(e.getMessage());
        }
    }

    //Delete Department
    @Override
    public void delete(Long departmentId) {
        try {
            departmentRepo.deleteById(departmentId);
        }catch (Exception e) {
            throw new ApiRequestException(e.getMessage());
        }
    }

    //Find Department By Id - can get null
    @Override
    public Department findById(Long departmentId) {
        try{
            Department department =  departmentRepo.getDepartmentByDepartmentId(departmentId).orElse(null);
            return department;
        }catch (Exception e){
            throw new ApiRequestException(e.getMessage());
        }
    }

    //Find Department By Code - can get null
    @Override
    public Department findByCode(String code) {
        try{
            return departmentRepo.getDepartmentByDepartmentCode(code).orElse(null);
        }catch (Exception e){
            throw new ApiRequestException(e.getMessage());
        }
    }

}
