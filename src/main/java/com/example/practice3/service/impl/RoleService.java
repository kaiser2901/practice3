package com.example.practice3.service.impl;

import com.example.practice3.DTO.PaginationModelRequest;
import com.example.practice3.entity.Role;
import com.example.practice3.exception.ApiRequestException;
import com.example.practice3.repo.RoleRepo;
import com.example.practice3.service.IRoleService;
import com.example.practice3.service.IValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Transactional
@Service
public class RoleService implements IRoleService {

    @Autowired
    private RoleRepo roleRepo;
    @Autowired
    private IValidationService validationService;

    @Override
    public Role findById(Long id) {
        return roleRepo.findById(id).orElse(null);
    }

    @Override
    public Role findByName(String name) {
        try {
            return roleRepo.findByRoleName(name).orElse(null);
        } catch (Exception e) {
            throw new ApiRequestException(e.getMessage());
        }
    }

    @Override
    public Role save(Role role) {
        try {
            return roleRepo.save(role);
        } catch (Exception e) {
            throw new ApiRequestException(e.getMessage());
        }
    }

    @Override
    public void delete(Long roleId) {
        try {
            roleRepo.deleteById(roleId);
        } catch (Exception e) {
            throw new ApiRequestException(e.getMessage());
        }
    }

//    @Override
//    public Collection<Role> findAllByName(Collection<String> username) {
//        try {
//            Collection<Role> roles = new ArrayList<>();
//            if(validationService.checkNullOrEmpty(username)){
//                return null;
//            }
//            for(String user : username){
//                Role role = roleRepo.findByName(user).orElse(null);
//                roles.add(role);
//            }
//
//            return roles;
//        } catch (Exception e) {
//            throw new ApiRequestException(e.getMessage());
//        }
//    }

    @Override
    public List<Role> getAll() {
        try {
            return roleRepo.findAll();
        } catch (Exception e) {
            throw new ApiRequestException(e.getMessage());
        }
    }

    @Override
    public Page<Role> getAllWithPagination(PaginationModelRequest paginationModelRequest) {
        try {
            return roleRepo.findAll(PageRequest.of(paginationModelRequest.getPage(),paginationModelRequest.getPageSize()));
        } catch (Exception e) {
            throw new ApiRequestException(e.getMessage());
        }
    }


}
