package com.example.practice3.repo;

import com.example.practice3.entity.FileImage;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface FileImageRepo extends JpaRepository<FileImage, String> {
    Optional<FileImage> findAllByEmployee_EmployeeId(Long fileId);
}
