package com.example.practice3.repo;

import com.example.practice3.entity.Department;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface DepartmentRepo extends JpaRepository<Department, Long> {
    Optional<Department> getDepartmentByDepartmentCode(String code);
    Optional<Department> getDepartmentByDepartmentId(Long id);
}
