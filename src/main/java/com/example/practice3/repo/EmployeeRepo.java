package com.example.practice3.repo;

import com.example.practice3.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;


public interface EmployeeRepo extends JpaRepository <Employee, Long> {
    Optional<Employee> findEmployeeByEmployeeId(Long id);
}
