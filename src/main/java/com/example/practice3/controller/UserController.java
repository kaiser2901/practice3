package com.example.practice3.controller;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.example.practice3.DTO.APIResponse;
import com.example.practice3.DTO.PaginationModelRequest;
import com.example.practice3.DTO.UserModelRequest;
import com.example.practice3.entity.Role;
import com.example.practice3.entity.User;
import com.example.practice3.exception.ApiRequestException;
import com.example.practice3.service.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private IUserService userService;
    @Autowired
    private IPaginationService paginationService;
    @Autowired
    private IUpsertUserService upsertUserService;
    @Autowired
    private IRefreshTokenService refreshTokenService;

    @GetMapping("/all")
    public ResponseEntity<APIResponse<List<User>>> getAll() {
        APIResponse apiException = APIResponse.builder()
                .recordCount(userService.getUsers().size())
                .response(userService.getUsers())
                .build();
        return new ResponseEntity<>(apiException, HttpStatus.OK);
    }

    @GetMapping("/pagination")
    public ResponseEntity<Page<User>> getAllWithPagination(@RequestParam int page, @RequestParam int size){
        PaginationModelRequest paginationModelRequest = new PaginationModelRequest(page, size);
        paginationModelRequest = paginationService.validatePagination(paginationModelRequest);
        Page<User> userPage = userService.getAllWithPagination(paginationModelRequest);
        return new ResponseEntity<>(userPage, HttpStatus.OK);
    }

    @GetMapping("/get-by-name/{name}")
    public ResponseEntity<User> getByName(@PathVariable("name") String name){
        return new ResponseEntity<>(userService.getUserByUsername(name), HttpStatus.OK);
    }

    //insert - update
    @PostMapping("/upsert")
    public ResponseEntity<User> upsert(@RequestBody UserModelRequest request){
        return new ResponseEntity<>(userService.upsert(upsertUserService.upsertUser(request)), HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") Long userId){
        userService.delete(userId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/token/refresh")
    public void refreshtoken(HttpServletRequest request, HttpServletResponse response) throws IOException {
        refreshTokenService.refreshtoken(request, response);
    }
}
