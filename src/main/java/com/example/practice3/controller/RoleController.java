package com.example.practice3.controller;

import com.example.practice3.DTO.APIResponse;
import com.example.practice3.DTO.PaginationModelRequest;
import com.example.practice3.DTO.RoleModelRequest;
import com.example.practice3.entity.Employee;
import com.example.practice3.entity.Role;
import com.example.practice3.service.IPaginationService;
import com.example.practice3.service.IRoleService;
import com.example.practice3.service.IValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/role")
public class RoleController {

    @Autowired
    private IRoleService roleService;
    @Autowired
    private IValidationService validationService;
    @Autowired
    private IPaginationService paginationService;

    @GetMapping("/all")
    public ResponseEntity<APIResponse<List<Role>>> getAll() {
        APIResponse apiResponse = APIResponse.builder()
                .response(roleService.getAll())
                .recordCount(roleService.getAll().size())
                .build();

        return new ResponseEntity<>(apiResponse, HttpStatus.OK);
    }

    @GetMapping("/pagination")
    public ResponseEntity<Page<Role>> getAllWithPagination(@RequestParam int page, @RequestParam int size){
        PaginationModelRequest paginationModelRequest = new PaginationModelRequest(page, size);
        paginationModelRequest = paginationService.validatePagination(paginationModelRequest);
        Page<Role> rolePage = roleService.getAllWithPagination(paginationModelRequest);
        return new ResponseEntity<>(rolePage, HttpStatus.OK);
    }

    @PostMapping("/upsert")
    public ResponseEntity<Role> upsert(RoleModelRequest request) {
        Role role = roleService.findById(request.getRoleId());
        if (validationService.checkNullOrEmpty(role)) {
            role = new Role();
        }
        role.setRoleName(request.getRoleName());
        return new ResponseEntity<>(role, HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> delete(RoleModelRequest request) {
        roleService.delete(request.getRoleId());
        return new ResponseEntity<>(HttpStatus.OK);
    }


}
