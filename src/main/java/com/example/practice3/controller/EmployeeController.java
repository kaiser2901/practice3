package com.example.practice3.controller;

import com.example.practice3.DTO.*;
import com.example.practice3.service.*;
import com.example.practice3.entity.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/employee")
public class EmployeeController {

    @Autowired
    private IEmployeeService employeeService;
    @Autowired
    private IUpsertEmployeeService upsertEmployeeService;
    @Autowired
    private IValidationService validationService;
    @Autowired
    private IPaginationService paginationService;
    @Autowired
    private IPaginationAndSortService paginationAndSortService;

    @GetMapping("/all")
    public ResponseEntity<APIResponse<List<Employee>>> getAll(){
        APIResponse aPiResponse = APIResponse.builder()
                .recordCount(employeeService.getAll().size())
                .response(employeeService.getAll())
                .build();
        return new ResponseEntity<>(aPiResponse, HttpStatus.OK);
    }

    @GetMapping("/pagination")
    public ResponseEntity<Page<Employee>> getAllWithPagination( @RequestParam int page, @RequestParam int size){
        PaginationModelRequest paginationModelRequest = new PaginationModelRequest(page, size);
        paginationModelRequest = paginationService.validatePagination(paginationModelRequest);
        Page<Employee> employeePage = employeeService.findWithPagination(paginationModelRequest);
        return new ResponseEntity<>(employeePage, HttpStatus.OK);
    }

    @GetMapping("/pagination-sort")
    public ResponseEntity<Page<Employee>> getAllWithPaginationAndSort
    ( @RequestParam int page,@RequestParam int size, @RequestParam String field) {
        PaginationAndSortModelRequest paginationAndSortModelRequest = new PaginationAndSortModelRequest( page , size, field);
        paginationAndSortModelRequest = paginationAndSortService.validationPaginationAndSort(paginationAndSortModelRequest);
        Page<Employee> employeePage = employeeService.findWithPaginationAndSorting(paginationAndSortModelRequest);
        return new ResponseEntity<>(employeePage, HttpStatus.OK);
    }

    @GetMapping("/find-by-id/{id}")
    public ResponseEntity<Employee> getById(@PathVariable("id") Long employeeId) {
        return new ResponseEntity<>(employeeService.findById(employeeId), HttpStatus.OK);
    }

    @PostMapping(value = "/upsert")
    public ResponseEntity<Employee> upsert(@RequestBody EmployeeModelRequest request) {
        return new ResponseEntity<>(upsertEmployeeService.upsert(request), HttpStatus.OK);
    }

    // Merge FileImage and Employee
    @PostMapping("/set-file")
    public ResponseEntity<Employee> mergeImage(@RequestBody MergeFileImageToEmployeeModelRequest request) {
        Employee employee = employeeService.mergeFileImage(request.getEmployeeId(), request.getFileImageId());
        return new ResponseEntity<>(employee,HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") Long employeeId) {
         employeeService.delete(employeeId);
         return new ResponseEntity<>(HttpStatus.OK);
    }
}
