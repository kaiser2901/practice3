package com.example.practice3.controller;

import com.example.practice3.entity.Department;
import com.example.practice3.DTO.DepartmentModelRequest;
import com.example.practice3.exception.ApiRequestException;
import com.example.practice3.service.IDepartmentService;
import com.example.practice3.service.IUpsertDepartmentService;
import com.example.practice3.service.impl.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/department")
public class DepartmentController {

    @Autowired
    private IDepartmentService departmentService;
    @Autowired
    private IUpsertDepartmentService upsertDepartmentService;

    //Get All Department
    @GetMapping("/all")
    public ResponseEntity<List<Department>> getAll() {
        return new ResponseEntity<>(departmentService.getAll(), HttpStatus.OK);
    }

    //Get Department By Code
    @GetMapping("/find-by-code/{code}")
    public ResponseEntity<Department> getByCode(@PathVariable("code") String departmentCode) {
        Department department = departmentService.findByCode(departmentCode);
        if(department != null){
            return new ResponseEntity<>(department,HttpStatus.OK);
        }else{
            throw new ApiRequestException("Can't find this department");
        }
    }

    //Get Department By Id
    @GetMapping("/find-by-id/{id}")
    public ResponseEntity<Department> getById(@PathVariable("id") Long departmentId) {
        Department department = departmentService.findById(departmentId);
        return new ResponseEntity<>(department,HttpStatus.OK);
    }

    //Upsert Department
    @PostMapping("/upsert")
    public  ResponseEntity<Department> upsert(@RequestBody DepartmentModelRequest request) {
        return new ResponseEntity<>(upsertDepartmentService.upsert(request),HttpStatus.OK);
    }

    //Delete Department
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") Long departmentId) {
        departmentService.delete(departmentId);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
