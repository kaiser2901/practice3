package com.example.practice3.controller;

import com.example.practice3.DTO.FileImageModelRequest;
import com.example.practice3.DTO.FileImageModelResponse;
import com.example.practice3.entity.FileImage;
import com.example.practice3.exception.ApiRequestException;
import com.example.practice3.service.IFileImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/file")
public class FileImageController {

    @Autowired
    private IFileImageService fileImageService;

    //Upload file
    @PostMapping("/upload")
    public ResponseEntity<String> upload(@RequestParam("storeImage") MultipartFile file) {

        try {
            FileImage fileImage = fileImageService.upload(file);
            return new ResponseEntity<>(fileImage.getFileId(), HttpStatus.OK);

        } catch (Exception e) {

            throw new ApiRequestException("Can't upload file, something went wrong...");

        }
    }

    //Get all file
    @GetMapping("/all")
    public ResponseEntity<List<FileImageModelResponse>> getAll() {
        List<FileImageModelResponse> files = fileImageService.getAll().map(
                dbFile -> {
                    String fileDownloadUri = ServletUriComponentsBuilder
                            .fromCurrentContextPath()
                            .path("/file/all")
                            .path(dbFile.getFileId())
                            .toUriString();

                    return new FileImageModelResponse(
                            dbFile.getFileId(),
                            dbFile.getFileName(),
                            fileDownloadUri,
                            dbFile.getFileType(),
                            dbFile.getFileData().length);
                }).collect(Collectors.toList());
        return ResponseEntity.status(HttpStatus.OK).body(files);
    }

    // Url Download
    @GetMapping("/all/{id}")
    public ResponseEntity<byte[]> getFile(@PathVariable("id") String id) {
        Optional<FileImage> optionalFileImage = fileImageService.findById(id);

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\""
                        + optionalFileImage.get().getFileName() + "\"")
                .body(optionalFileImage.get().getFileData());
    }

    //Get Url Avatar
    @GetMapping("/get-uri/{id}")
    public ResponseEntity<String> getUri(@PathVariable("id") String fileId) {
        FileImageModelRequest fileImage = fileImageService.findFileImageById(fileId);

        String linkUri = ServletUriComponentsBuilder
                .fromCurrentContextPath()
                .path("/file/all/")
                .path(fileImage.getFileId())
                .toUriString();

        return new ResponseEntity<>(linkUri, HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") String fileId) {
        fileImageService.delete(fileId);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
