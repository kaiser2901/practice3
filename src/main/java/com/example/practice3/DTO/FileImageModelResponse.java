package com.example.practice3.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class FileImageModelResponse {

    private String id;

    private String name;

    private String url;

    private String type;

    private long size;

    public FileImageModelResponse(String name, String url, String type, long size) {
        this.name = name;
        this.url = url;
        this.type = type;
        this.size = size;
    }
}
