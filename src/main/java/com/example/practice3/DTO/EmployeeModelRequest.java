package com.example.practice3.DTO;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class EmployeeModelRequest {
    private Long employeeId;
    private String employeeName;
    private String employeePhone;
    private String employeeEmail;
    private String employeeCode;
    private Long departmentId;
    private String fileImageId;
}
