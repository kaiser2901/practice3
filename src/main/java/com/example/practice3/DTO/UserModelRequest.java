package com.example.practice3.DTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Collection;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class UserModelRequest {
    private Long userId;
    private String userName;
    private String userUsername;
    private String userPassword;
    private Collection<String> roleName;
}
