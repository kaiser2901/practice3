package com.example.practice3.DTO;

import com.fasterxml.jackson.annotation.JsonView;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Lob;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FileImageModelRequest {

    @JsonView(FileImageRequest.class)
    private String fileId;

    @JsonView(FileImageRequest.class)
    private String fileName;

    @JsonView(FileImageRequest.class)
    private String fileType;

    @JsonView(FileImageRequest.class)
    @Lob
    private byte[] fileData;

}
