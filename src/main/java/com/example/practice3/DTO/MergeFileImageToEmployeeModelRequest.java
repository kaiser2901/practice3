package com.example.practice3.DTO;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MergeFileImageToEmployeeModelRequest {

    private Long employeeId;

    private String fileImageId;
}
