package com.example.practice3.DTO;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PaginationAndSortModelRequest {

    private int page;

    private int pageSize;

    private String field;
}
