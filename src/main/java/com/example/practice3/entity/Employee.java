package com.example.practice3.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;


@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class Employee implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, updatable = false)
    private Long employeeId;
    private String employeeName;
    private String employeePhone;
    private String employeeEmail;
    private String employeeCode;

    @JsonManagedReference
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "employeeAvatar", referencedColumnName = "fileId")
    private FileImage fileImage;

    @JsonManagedReference
    @ManyToOne()
    @JoinColumn(name = "departmentCode", referencedColumnName = "departmentCode")
    private Department department;

}
